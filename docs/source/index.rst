RPS - WAF
=========

.. image:: https://code.europa.eu/digit-c4/rps/waf/badges/main/pipeline.svg?style=flat-square
   :target: https://code.europa.eu/digit-c4/rps/waf/-/commits/main
   :alt: Pipeline Status

.. image:: https://code.europa.eu/digit-c4/rps/waf/-/badges/release.svg?style=flat-square
   :target: https://code.europa.eu/digit-c4/rps/waf/-/releases
   :alt: Latest Release

Introduction
------------

You will find here information about the RPS WAF Docker Images.

Content
-------

.. toctree::
   :maxdepth: 2

   architecture/index
   coraza/index
   modsecurity/index

See Also
--------

* `Config Controller <https://digit-c4.pages.code.europa.eu/rps/config-controller/>`_
* `Docker image for RPS <https://digit-c4.pages.code.europa.eu/rps/proxy/>`_
* `Ansible Collection for RPS <https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-collection>`_
* `Ansible Playbooks for RPS <https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-playbooks>`_
* `AWX job provisionning <https://digit-c4.pages.code.europa.eu/awx-data/>`_
