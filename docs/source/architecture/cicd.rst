Continuous Integration
======================

Every commit will run a build of the docker image, which runs the test suite.
If the build succeeds, the final image is guaranteed to be valid (in theory).

When pushed on the ``main`` branch, the image is pushed to the Gitlab registry
with the following tags:

* ``$CI_COMMIT_SHORT_SHA``, e.g.: ``digit-c4/rps/waf/<waf tech>:a1b2c3d4``
* ``latest``, e.g.: ``digit-c4/rps/waf/<waf tech>:latest``

**NB:** A job is also run at the end of the pipeline to remove old images from
the CI runner (for disk space efficiency). At least 3 images are kept, so that
next builds can benefit from the cached layers.
