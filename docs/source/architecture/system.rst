System Requirements
===================

Host
----

To run a WAF docker container, you will need a virtual machine with the
following requirements:

* **OS:** Debian 12
* **CPU:** 8 cores
* **RAM:** 16 GB
* **Applications:** Netbox Docker Agent

Environment
-----------

The Docker container expects the following environment variables to be set:

.. csv-table::
   :header: "Variable", "Description", "Example"

   ``NETBOX_API``, "The URL of the Netbox API", "``http://netbox.ec.local:8080``"
   ``NETBOX_TOKEN``, "The Netbox API token", "N/A"
   ``EC_RPS_ALLOWED_ORIGINS``, "Comma separated list of network addresses, or ranges, that are allowed to access ``/snet/agent`` URLs (defaults to all)", "``*``"
   ``EC_BUBBLE``, "The name of the bubble", "``cop``"
   ``EC_ENV``, "The name of the environment", "``copbeta``"
   ``EC_RPS_ACME_ENABLED``, "Whether mappings certificates are provided by an ACME server", "``true``"
   ``EC_RPS_PROXY_HOST``, "The hostname of the Reverse Proxy service"
   ``EC_RPS_WAF_MODE``, "Wether to run the WAF in 'detection only' mode or 'blocking' mode (invalid values assume 'blocking' mode)", "``warning`` or ``blocking``"
   ``EC_RPS_WAF_AUDITLOG``, "Enable/Disable the WAF audit log", "``true``"
   ``EC_RPS_WAF_USE_CRS``, "Use only the default CoreRuleSet", "``true``"
   ``EC_RPS_WAF_USE_CRS_*``, "Include specific ruleset file from the CoreRuleSet (ignored if ``EC_RPS_WAF_USE_CRS`` is true)", "``REQUEST-933-APPLICATION-ATTACK-PHP``"
   ``EC_RPS_WAF_USE_CUSTOM_*``, "Include specific ruleset file from the European Commission RuleSet (ignored if ``EC_RPS_WAF_USE_CRS`` is true)", "``docker-registry``"
   ``EC_RPS_WAF_USE_RULE_*``, "ModSecurity directive (ignored if ``EC_RPS_WAF_USE_CRS`` is true)", "``SecRule ...``"
   ``DEBUG``, "If enabled, will run NGINX+ in debug mode and sets log levels to DEBUG as well", "``false``"
   ``EC_RPS_PROXY_PROTOCOL``, "enable/disable the PROXY protocol on the WAF layer", "``true``"
   ``EC_RPS_UPSTREAM_HAS_TLS``, "enable/disable HTTPS for the next layer", "``false``"
   ``EC_RPS_WAF_BODY_ACCESS``, "enable/disable request body inspection in the waf", "``true``"
   ``EC_RPS_WAF_ALLOWED_RCT``, "Specify the request content types allowed by the waf", "``|application/x-www-form-urlencoded| |multipart/form-data| |multipart/related| |text/xml| |application/xml| |application/soap+xml| |application/json| |application/cloudevents+json| |application/cloudevents-batch+json|``"
   ``EC_RPS_WAF_RESTRICTED_EXTENSION``, "Specify the restricted extensions for the waf", "``.asa/ .asax/ .ascx/ .axd/ .backup/ .bak/ .bat/ .cdx/ .cer/ .cfg/ .cmd/ .com/ .config/ .conf/ .cs/ .csproj/ .csr/ .dat/ .db/ .dbf/ .dll/ .dos/ .htr/ .htw/ .ida/ .idc/ .idq/ .inc/ .ini/ .key/ .licx/ .lnk/ .log/ .mdb/ .old/ .pass/ .pdb/ .pol/ .printer/ .pwd/ .rdb/ .resources/ .resx/ .sql/ .swp/ .sys/ .vb/ .vbs/ .vbproj/ .vsdisco/ .webinfo/ .xsd/ .xsx/``"
   ``EC_RPS_WAF_RESTRICTED_HEADERS``, "Specify the restricted headers for the waf", "``/proxy/ /lock-token/ /content-range/ /if/``"
   ``EC_RPS_WAF_ALLOWED_METHOD``, "Specify the allowed HTTP Method for the waf", "``GET PUT PATCH DELETE HEAD POST OPTIONS``"
   ``EC_RPS_WAF_ALLOWED_FILES``, "Specify the allowed extensions files for the waf", "``.json .gitlab .bash``"

Persistent Volumes
------------------

The following volumes are expected to be mounted:

.. csv-table::
   :header: "Path in container", "Description"

   ``/data/certs``, "The directory where the certificates are stored"
   ``/var/www/acme-challenge``, "The directory where the ACME challenge files are served from"

When ACME is enabled
~~~~~~~~~~~~~~~~~~~~

The following certificates are expected to be present in the ``/data/certs/acme``:

* ``<domain name>.pub`` for the certificate's public key
* ``<domain name>.key`` for the certificate's private key

When ACME is NOT enabled
~~~~~~~~~~~~~~~~~~~~~~~~

The following certificates are expected to be present in the ``/data/certs``:

* ``<domain name>.pub`` for the certificate's public key
* ``<domain name>.key`` for the certificate's private key

Netbox
------

The following plugins are required to be installed in Netbox:

* Netbox Plugin RPS
