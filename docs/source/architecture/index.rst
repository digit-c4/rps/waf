Architecture
============

Deployment of the WAF docker container is done via the Netbox Docker plugin.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   cicd
   system
   design
