import sys

from diagrams import Diagram
from diagrams.c4 import Container, Relationship, System, SystemBoundary, Person


with Diagram("Architecture", direction="TB", filename=sys.argv[1], show=False):
    user = Person(
        name="User",
        description="Browse EC applications",
        external=True,
    )

    with SystemBoundary("Bubble"):
        lb = Container(
            "Load Balancer",
            technology="Traefik",
            description="Route traffic",
        )

        with SystemBoundary("Acceptance"):
            waf_acc = Container(
                "WAF",
                technology="ModSecurity",
                description="Protects applications",
            )

            rps_acc = Container(
                "Reverse Proxy",
                technology="NGINX+",
                description="Proxy traffic to applications"
            )

        with SystemBoundary("Production"):
            waf_prod = Container(
                "WAF",
                technology="ModSecurity",
                description="Protects applications",
            )

            rps_prod = Container(
                "Reverse Proxy",
                technology="NGINX+",
                description="Proxy traffic to applications"
            )

    with SystemBoundary("Client"):
        app_acc = System(
            "Application",
            description="Acceptance environment",
            external=True,
        )

        app_prod = System(
            "Application",
            description="Production environment",
            external=True,
        )

    user >> Relationship("Browse") >> lb
    lb >> Relationship("Route") >> waf_acc
    lb >> Relationship("Route") >> waf_prod

    waf_acc >> Relationship("Protect") >> rps_acc
    waf_prod >> Relationship("Protect") >> rps_prod

    rps_acc >> Relationship("Proxy") >> app_acc
    rps_prod >> Relationship("Proxy") >> app_prod
