Container Architecture
======================

The ``digit-c4/rps/waf/modsec`` WAF docker image is based on the Apache2
webserver.

Logging
-------

Each process present in the container will produce logs, by using
`procfusion <https://github.com/linkdd/procfusion>`_, those different sources
are prefixed, allowing proper filtering and log collection.

.. csv-table::
   :header: "Source", "Stream", "Prefix Regex", "Message format (after prefix)"

   "Apache2 Logs", "stdout & stderr", "``^proc\.webserver\[(stdout|stderr)\]\s+\|``", "logfmt"
   "Config Controller Logs", "stdout & stderr", "``^proc\.configctl\[(stdout|stderr)\]\s+\|``", "logfmt"
   "Process manager Logs", "stdout & stderr", "``^controller\[(stdout|stderr)\]\s+\|``", "logfmt"

**NB:** While most of those sources use the same structured logging format
(``logfmt``), they don't log the same fields.

Management URLs
---------------

The WAF will provide the following management URLs on the default virtual host:

.. csv-table::
   :header: "URL", "Description"

   ``/snet/apache/status``, "Apache2 Status Page"
   ``/snet/apache/info``, "Apache2 Server Info Page"
   ``/snet/agent``, "Config Controller API for Netbox webhooks"
