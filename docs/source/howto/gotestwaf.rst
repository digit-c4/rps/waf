GoTestWaf
==========

The 'gotestswaf' pipeline automates the process of testing WAF by sending various types of requests
to identify its vulnerabilities or weaknesses. It is designed to test the effectiveness of a WAF against malicious requests
and generate a report, summarizing its results.

.. list-table:: Pipeline Variables
   :widths: 25 50
   :header-rows: 1

   * - Variables
     - Description
   * - WAF_PIPELINE
     - The name of the pipeline execution.it should be 'gotestwaf'.
   * - URL_TO_TEST
     - The URL of the target application that is protected by the WAF, This is the endpoint against which gotestwaf will send its test requests.
   * - WAF_NAME
     - The name of theWAF being tested, This will be included in the final report to identify which WAF was evaluated.


GoTestWAF Execution:
-------------------

  -	The pipeline runs the GoTestWAF tool against the provided URL_TO_TEST.
  -	The tool will execute a series of tests designed to probe the WAF's security rules and policies.
  -	During this phase, requests are sent to the URL, and the WAF's responses are analyzed.


Pipeline Execution Guide
------------------------

1. **Access the project & Run the pipeline**:
   -  Navigate to the Gitlab project contains the gotestwaf pipeline "https://code.europa.eu/digit-c4/rps/waf" and go to CI/CD section and select Pipelines and click the RUN pipeline.
2.	**In the Run template, provide the following variables**:
   -	WAF_PIPELINE: Choose 'gotestawf' pipeline from the list.
   -	URL_TO_TEST: Provide the URL of the application you want to test ( usually we use "https://test-httpbin.bubble_name.nms.tech.ec.europa.eu" )
   -	WAF_NAME: Enter the name of the WAF under test ( coraza_$version, modsec_$version).
3.	**Report Generation**:
   -	After testing is complete, GoTestWAF generates a report summarizing the test results.
   -	The report includes a score that represents the effectiveness of the WAF in blocking malicious requests.
   -	Detailed information about requests that were not blocked is also included.  
4.	**Upload Report**:
   -	The pipeline automatically uploads the generated report to the GitLab CI/CD interface.
   -	The report can be accessed via the pipeline’s artifacts section.