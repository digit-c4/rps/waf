# Web Application Firewall

This repository contains the Docker Image for the following WAFs:

 - Caddy+Coraza
 - Apache2+ModSecurity

For more information:

 - the [CONTRIBUTING](./CONTRIBUTING.md) document describes how to contribute to the repository
 - consult the [documentation](https://digit-c4.pages.code.europa.eu/rps/waf)
