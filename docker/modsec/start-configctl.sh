#!/bin/sh

set -e

source /opt/ec-rps-config-controller/venv/bin/activate

export EC_RPS_CERTS_PATH="/data/certs"
export EC_RPS_WEBHOOK_SCOPE="waf"
export EC_RPS_CONFIG_MODULE="ec_rps_config_waf_modsecurity"

sleep 5

exec python -m ec_rps_config_controller
