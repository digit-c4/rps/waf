#!/bin/sh

GOTEMPL=${GOTEMPL:-gotempl}
PREFIX=${PREFIX:-}

# Configure WAF variables
${GOTEMPL} -t ${PREFIX}/etc/apache2/waf.d/00-core.template -o ${PREFIX}/etc/apache2/waf.d/10-core.conf
${GOTEMPL} -t ${PREFIX}/etc/apache2/waf.d/10-setup.template -o ${PREFIX}/etc/apache2/waf.d/10-setup.conf

# Configure WAF ruleset
case "$EC_RPS_WAF_USE_CRS" in
  t|y|true|yes|on|1)
    echo "Include /etc/coreruleset/rules/*.conf" > ${PREFIX}/etc/apache2/waf.d/20-rules.conf
    ;;

  *)
    printenv | grep -E '^EC_RPS_WAF_USE_CRS_' | while read -r line
    do
      label=$(echo "$line" | cut -d= -f1 | sed -e 's/^EC_RPS_WAF_USE_CRS_//')
      value=$(echo "$line" | cut -d= -f2-)

      echo "scope=waf action=include from=coreruleset label=${label} file=${value}"
      echo "Include /etc/coreruleset/rules/${value}.conf" >> ${PREFIX}/etc/apache2/waf.d/20-rules.conf
    done

    printenv | grep -E '^EC_RPS_WAF_USE_CUSTOM_' | while read -r line
    do
      label=$(echo "$line" | cut -d= -f1 | sed -e 's/^EC_RPS_WAF_USE_CUSTOM_//')
      value=$(echo "$line" | cut -d= -f2-)

      echo "scope=waf action=include from=custom label=${label} file=${value}"
      echo "Include /etc/ec-waf-ruleset/${value}.conf" >> ${PREFIX}/etc/apache2/waf.d/20-rules.conf
    done

    printenv | grep -E '^EC_RPS_WAF_USE_RULE_' | while read -r line
    do
      label=$(echo "$line" | cut -d= -f1 | sed -e 's/^EC_RPS_WAF_USE_RULE_//')
      value=$(echo "$line" | cut -d= -f2-)

      echo "scope=waf action=rule label=${label}"
      echo "${value}" >> ${PREFIX}/etc/apache2/waf.d/20-rules.conf
    done
    ;;
esac

# Configure data files
for file_data in $EC_RPS_WAF_ALLOWED_FILES
do
    # espace special characters
    data_escaped=$(echo "$file_data" | sed 's/[\/&]/\\&/g')
    sed -i "/$data_escaped/d" ${PREFIX}/etc/coreruleset/rules/restricted-files.data
done
