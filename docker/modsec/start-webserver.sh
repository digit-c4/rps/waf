#!/bin/sh

set -e

mkdir -p /data/certs/acme
mkdir -p /data/certs/static

sh /configure-waf.sh

# Start the web server
httpd -DFOREGROUND &
sleep 1

# Wait for the web server to exit
PIDFILE="/var/run/apache2/httpd.pid"
while [ -f "$PIDFILE" ] && kill -0 $(cat "$PIDFILE") 2> /dev/null
do
  sleep 1
done

killall -9 httpd
