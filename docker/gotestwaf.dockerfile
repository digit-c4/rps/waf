ARG GOTESTWAF_VERSION="0.5.5"

FROM wallarm/gotestwaf:${GOTESTWAF_VERSION}

USER root

WORKDIR /app

ADD docker/gotestwaf/report_template.html ./pkg/report/report_template.html
