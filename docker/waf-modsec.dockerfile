ARG ALPINE_VERSION="3.20"
ARG MODSECURITY_VERSION="2.9.7"
ARG CORERULESET_VERSION="3.3/dev"
ARG PROCFUSION_VERSION="0.2.2"
ARG EC_RPS_CONFIG_CONTROLLER_VERSION="0.24.3"
ARG GOTEMPL_VERSION="0.7.1"

###############################################################################
## BUILD MOSECURITY APACHE PLUGIN
###############################################################################

FROM alpine:${ALPINE_VERSION} AS modsec-builder
ARG MODSECURITY_VERSION

RUN set -ex && \
    apk add --no-cache \
        git \
        autoconf \
        automake \
        build-base \
        libtool \
        apache2-dev \
        curl-dev \
        libxml2-dev \
        yajl-dev \
        pcre-dev \
        lua5.3-dev \
        linux-headers \
        wget \
      && \
    git clone https://github.com/owasp-modsecurity/ModSecurity -b v${MODSECURITY_VERSION} --depth=1 /workspace/modsec && \
    cd /workspace/modsec && \
    ./autogen.sh && \
    ./configure && \
    make && \
    make install DESTDIR=/build

###############################################################################
## DOWNLOAD AND EXTRACT OWASP CORE RULESET
###############################################################################

FROM alpine:${ALPINE_VERSION} AS crs-builder
ARG CORERULESET_VERSION

RUN set -ex && \
    apk add --no-cache git && \
    git clone https://github.com/coreruleset/coreruleset -b v${CORERULESET_VERSION} --depth=1 /build/etc/coreruleset && \
    cd /build/etc/coreruleset

###############################################################################
## BUILD EC-RPS-CONFIG-CONTROLLER
###############################################################################

ARG ALPINE_VERSION
FROM python:3.12-alpine${ALPINE_VERSION} AS venv-builder
ARG EC_RPS_CONFIG_CONTROLLER_VERSION

RUN set -ex && \
    apk add --no-cache git && \
    python -m venv /opt/ec-rps-config-controller/venv && \
    source /opt/ec-rps-config-controller/venv/bin/activate && \
    pip install -e git+https://code.europa.eu/digit-c4/rps/config-controller.git@v${EC_RPS_CONFIG_CONTROLLER_VERSION}#egg=ec-rps-config-controller

###############################################################################
## DOWNLOAD AND EXTRACT PROCFUSION
###############################################################################

ARG ALPINE_VERSION
FROM alpine:${ALPINE_VERSION} AS procfusion-builder
ARG PROCFUSION_VERSION

ADD https://github.com/linkdd/procfusion/releases/download/v${PROCFUSION_VERSION}/procfusion-v${PROCFUSION_VERSION}-x86_64-unknown-linux-musl.tar.gz /
RUN set -x && \
    tar -xzf procfusion-v${PROCFUSION_VERSION}-x86_64-unknown-linux-musl.tar.gz && \
    mv procfusion-v${PROCFUSION_VERSION}-x86_64-unknown-linux-musl/procfusion /usr/local/bin/procfusion && \
    rm -rf procfusion-v${PROCFUSION_VERSION}-x86_64-unknown-linux-musl.tar.gz procfusion-v${PROCFUSION_VERSION}-x86_64-unknown-linux-musl && \
    chmod +x /usr/local/bin/procfusion

###############################################################################
## DOWNLOAD AND EXTRACT Gotempl
###############################################################################

ARG ALPINE_VERSION
FROM alpine:${ALPINE_VERSION} AS gotempl-builder
ARG GOTEMPL_VERSION
ADD https://github.com/link-society/gotempl/releases/download/v${GOTEMPL_VERSION}/gotempl-v${GOTEMPL_VERSION}-linux-amd64.tar.gz /
RUN set -x && \
    tar -xzf gotempl-v${GOTEMPL_VERSION}-linux-amd64.tar.gz && \
    mv gotempl /usr/local/bin/gotempl && \
    rm -rf gotempl-v${GOTEMPL_VERSION}-linux-amd64.tar.gz && \
    chmod +x /usr/local/bin/gotempl

###############################################################################
## FINAL ARTIFACT
###############################################################################

ARG ALPINE_VERSION
FROM python:3.12-alpine${ALPINE_VERSION} AS runner
COPY --from=modsec-builder /build /
COPY --from=crs-builder /build /
COPY --from=venv-builder /opt/ec-rps-config-controller/venv /opt/ec-rps-config-controller/venv
COPY --from=procfusion-builder /usr/local/bin/procfusion /usr/local/bin/procfusion
COPY --from=gotempl-builder /usr/local/bin/gotempl /usr/local/bin/gotempl

RUN set -ex && \
    apk add --no-cache \
        apache2 apache2-ctl \
        apache2-proxy apache2-ssl apache2-http2 \
        libcurl libxml2 yajl pcre lua5.3 && \
    mkdir -p /var/www/acme-challenge && \
    chown -R apache:apache /var/www/acme-challenge && \
    chmod 755 /var/www/acme-challenge && \
    rm -rf /etc/apache2/conf.d

COPY --from=modsec-builder /workspace/modsec/unicode.mapping /etc/apache2/waf.d/unicode.mapping

ADD docker/modsec/ec-waf-ruleset /etc/ec-waf-ruleset
ADD docker/modsec/apache2/httpd.conf /etc/apache2/httpd.conf
ADD docker/modsec/apache2/conf.d /etc/apache2/conf.d
ADD docker/modsec/apache2/waf.d /etc/apache2/waf.d

ADD docker/modsec/procfusion.toml /etc/procfusion.toml
ADD docker/modsec/start-configctl.sh /start-configctl.sh
ADD docker/modsec/configure-waf.sh /configure-waf.sh
ADD docker/modsec/start-webserver.sh /start-webserver.sh
RUN chmod +x /start-configctl.sh /start-webserver.sh

ENV NETBOX_API=""
ENV NETBOX_TOKEN=""

ENV EC_BUBBLE=""
ENV EC_ENV=""

ENV EC_RPS_ALLOWED_ORIGINS="*"
ENV EC_RPS_ACME_ENABLED="true"
ENV EC_RPS_PROXY_HOST=""
ENV EC_RPS_WAF_MODE="blocking"
ENV EC_RPS_WAF_AUDITLOG="true"
ENV EC_RPS_WAF_USE_CRS="true"
ENV EC_RPS_WAF_BODY_ACCESS="true"
ENV EC_RPS_WAF_ALLOWED_RCT=""
ENV EC_RPS_WAF_RESTRICTED_EXTENSION=""
ENV EC_RPS_WAF_ALLOWED_METHOD=""
ENV EC_RPS_WAF_RESTRICTED_HEADERS=""
ENV EC_RPS_WAF_ALLOWED_FILES=""

ENTRYPOINT ["/usr/local/bin/procfusion", "/etc/procfusion.toml"]
